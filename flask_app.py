# A very simple Flask Hello World app for you to get started with...

from flask import Flask,render_template, request,redirect,url_for
from pony.orm import Database, Required, select, db_session, PrimaryKey
from person import Person

app = Flask(__name__)
db = Database()


class Students(db.Entity):
    name = Required(str)
    surname = Required(str)
    grade = Required(int)

class Products(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    quantity = Required(int)
    price = Required(int)

db.bind(provider='sqlite', filename='productdb', create_db=True)
db.generate_mapping(create_tables=True)


@app.route('/')
def index():

    return render_template('index.html')

@app.route('/Table')
def tabel():
    x = [ Person('Ana', 'Tirana', 'Developer'),
          Person('Ana', 'Tirana', 'Developer'),
          Person('Ana', 'Tirana', 'Developer'),
          Person('Ana', 'Tirana', 'Developer'),
          Person('Ana', 'Tirana', 'Developer'),]
    return render_template('table.html', PEOPLE = x)

things = [{'task_name':'learn', 'duration': 10, 'place': 'home'},
                {'task_name':'cook', 'duration': 3, 'place': 'kitchen'}]


@app.route('/todo', methods=['GET', 'POST'])
def todo():
    if request.method == 'GET':
         return render_template('todo.html',TODO = things )

    elif request.method == 'POST':
        new_dict = { 'task_name' : request.form.get('task_name'),
                     'duration' : request.form.get('duration'),
                     'place' : request.form.get('place')}
        things.append(new_dict)
        return render_template('todo.html', TODO = things )


@app.route('/grades', methods=['GET', 'POST'])
@db_session
def grades():
    if request.method == 'GET':



        return render_template('class_grades.html' )

    elif request.method == 'POST':
        name = request.form.get('name')
        surname = request.form.get('surname')
        grade = request.form.get('grade')

        Students(name=name, surname=surname, grade=grade)

        students = list(select(t for t in Students))

        return render_template('class_grades.html', STUDENTS = students )

@app.route('/products', methods=['GET', 'POST'])
@db_session
def product():
     if request.method == 'GET':
         product_name = request.args.get('product_name', '')

         query1 = select(p for p in Products if product_name.upper() in p.name.upper())

         return render_template('product_table.html',  PRODUCTS = query1 )


     elif request.method == 'POST':

         name = request.form.get('name')
         quantity = request.form.get('quantity')
         price = request.form.get('price')

         Products(name=name, quantity=quantity, price=price)

         return redirect(url_for('product'))


@app.route('/products/delete/<id>', methods=['GET'])
@db_session
def delete_product(id):

   P_delete = Products.get(id = id) # -> object. / None
   if P_delete :
       P_delete.delete()
   return redirect(url_for('product'))

@app.route('/products/modify/<id>', methods=['GET', 'POST'])
@db_session
def modify_product(id):
    if request.method == 'GET':

        current_product = Products.get(id=id)

        #name, quantity, price = db.get("select name, quantity, price from Products where id = $id")
        return render_template('modify_product.html', PRODUCT=current_product)


    elif request.method == 'POST':
         name = request.form.get('name')
         quantity = request.form.get('quantity')
         price = request.form.get('price')

         Products[id].set(name = name , price = price, quantity = quantity)

         return redirect(url_for('product'))
    #return render_template('modify_product.html', name = name , price = price, quantity = quantity)
if __name__ == '__main__':
    app.run(threaded=True, port=5000)
